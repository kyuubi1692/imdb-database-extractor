from bs4 import BeautifulSoup
import re
from robobrowser import RoboBrowser

log_file_name = 'log.txt'

DESKTOP = 1
MOBILE = 2

interface = MOBILE

def getGenre(html):
	# print "GETTING genre"
	genre = []
	if(interface == DESKTOP):
		markup = set(re.findall(r'<span class="itemprop" itemprop="genre">[A-Za-z]*</span>',html))
		for g in markup:
			soup = BeautifulSoup(g)
			genre.append(soup.get_text().strip())
		
	else :
		markup = re.findall(r'<span class="itemprop" itemprop="genre">[A-Za-z]*</span>',html)
		for g in markup:
			soup = BeautifulSoup(g)
			genre.append(soup.get_text().strip())
	genre = set(genre)
	# print "Returning Genre"
	return genre

def getName(html):
	# print "GETTING nAME"
	if(interface == DESKTOP):
		markup = re.search(r'<span class="itemprop" itemprop="name">(.*)</span>',html)
		markup = str(markup.group())
		soup = BeautifulSoup(markup)
		name = soup.get_text().strip()
	else :
		markup = re.search(r'<div class="media-body">.*?<small',html,re.DOTALL)
		markup = str(markup.group())
		markup = re.search(r'<h1>.*<',markup,re.DOTALL)
		name = str(markup.group())[4:-1].strip()
		name.strip()
	# print "Returning Name"
	return name
		
	

def getRating(html):
	try:
		if(interface == DESKTOP):
			markup = re.search(r'<div class="titlePageSprite star-box-giga-star"> (.*) </div>', html)
			markup = str(markup.group())
			soup = BeautifulSoup(markup)
			rating = soup.get_text().strip()
		else:
			markup = re.search(r'<span class="inline-block text-left vertically-middle">(.*)<small class="text-muted">', html)
			markup = str(markup.group())
			markup = re.search(r'>(.*)<',markup)
			rating = str(markup.group())[1:-1].strip()
		# print "Returning Rating"
		return rating
	except Exception, e:
		return "0.0"

	

def getYear(html):
	# print "GETTING Year"
	if(interface == DESKTOP):
		markup 	= re.search(r'<a href="/year/\d+(.)*>\d+</a>',html)
		markup 	= str(markup.group())
		print markup
		markup 	= re.search(r'>(.*)<',markup)
		year 	= str(markup.group())[1:-1].strip()
	else :

		markup = re.search(r'<small class="sub-header">[\s\d()a-zA-Z0-9<>"\']*</',html)
		markup = str(markup.group())
		markup = re.search(r'\([\d]*\)',markup)
		year = str(markup.group())[1:-1]
	# print "Returning Year"
	return year

def updateLog(x):
	fout =open(log_file_name, "w")
	fout.write(str(x))
	fout.close()

def getLastIDFromLog():
	try:
		fout = open(log_file_name,"r")
		x = fout.read()
		return int(x),"a"
	except Exception, e:
		print "Could not find Log File. Recreating Log File"
		return 0,"w"
class Movie(object):
	"""Main Movie Object"""
	def __init__(self, name, year, rating):
		super(Movie, self).__init__()
		self.name = name
		self.year = year
		self.rating = rating
		

browser = RoboBrowser(history=False)
base_url = 'http://m.imdb.com/title/tt'

movie_id = 1;
movieByGenre = dict()
retries = 0
startID,file_mode = getLastIDFromLog()
for x in xrange(startID+1,startID+1000000):
	try:
		browser.open("%s%07d/" %(base_url,x))	
		html = str(browser.find_all())
		if(html.find('404 Error - IMDb') != -1):
			print "continuing :", x
			continue
		genres, name, year, rating = [getGenre(html),getName(html),getYear(html),getRating(html)]
		# movieByGenre[genre] = Movie(name, year, rating, genre)
		name = unicode(name, encoding='utf-8')
		# print name, rating, year
		for genre in genres:
			print ("%-15s [%20s %%]"%(genre,str((x*100)/1840309.0)))
			fout = open(genre+".txt", file_mode)
			writeStr = str(x) + "," 
			writeStr += name+","
			writeStr += rating+","+year+","
			writeStr +=genre.encode('utf8')+"\n"
			fout.write(writeStr.encode('utf8'))
			fout.close()
		updateLog(x)
		retries = 0
	except Exception, e:
		print e,"\n","Now Retrying"
		if(retries < 5):
			x -= 1
		else:
			print "Skipping Title :", x
			retries = 0
		retries += 1
