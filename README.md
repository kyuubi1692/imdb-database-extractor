- Extract Name, Year, Rating of Movies and save it in the respective Genre.

- Separate File generated for each genre, and the movie tuple is appended to each genre the movie belongs to.

- Resume enabled incase of any failure.

TO ADD
---------------
- Multi Thread Request
- Handle Time Out
- Modules to extract other details of the movie

TO DO (MITRA) :
---------------
- Set up pyGraph and GraphViz to create graph
- Set up a hashmap (or a bloom filter) to figure out already mined movies. (will help it scale)
- The analytics part ;-)
